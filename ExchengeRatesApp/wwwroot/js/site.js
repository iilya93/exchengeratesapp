﻿$body = $("body");

$(document).on({
    ajaxStart: function () { $body.addClass("loading"); },
    ajaxStop: function () { $body.removeClass("loading"); }
});

$(document).ready(function () {

    var sineRenderer = null;
    var title = null;
    var lines = [];
    var chart1 = null;

    var message = $(".alert");
   
    function GetExchangeRates(baseCurrency, start, end, target) {
        message.removeClass("show");

        $.ajax({
            url: '/Home/GetExchangeRates' + '?BaseCurrency=' + baseCurrency + '&TargetCurrency=' + target + '&Start=' + start + '&End=' + end,
            type: 'GET',
            contentType: "application/json",
            success: function (result) {
                if (result.isError) {
                    console.log(result.message);
                    message.children("p").remove();
                    message.append('<p>' + result.message + '</p>');
                    message.addClass("show");
                }
                else {
                    message.removeClass("show");

                    sineRenderer = result.values;
                    lines = result.lines;
                    title = result.title;
                    if (chart1 !== null && chart1 !== undefined) {
                        chart1.destroy();
                    }

                        chart1 = $.jqplot('chart1', sineRenderer, {
                            title: result.title,
                            axes: {
                                xaxis: {
                                    renderer: $.jqplot.DateAxisRenderer,
                                    rendererOptions: {
                                        tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                    },
                                    tickOptions: {
                                        fontSize: '10pt',
                                        fontFamily: 'Tahoma',
                                        angle: -40
                                    }
                                },
                                yaxis: {
                                    rendererOptions: {
                                        tickRenderer: $.jqplot.CanvasAxisTickRenderer
                                    },
                                    tickOptions: {
                                        fontSize: '10pt',
                                        fontFamily: 'Tahoma',
                                        angle: 30
                                    }
                                }
                            },
                            legend: {
                                show: true,
                                placement: 'outsideGrid',
                                renderer: $.jqplot.EnhancedLegendRenderer,
                                labels: lines,
                                location: "s",
                                rowSpacing: "0px",
                                rendererOptions: {
                                    numberRows: 10,
                                    seriesToggle: 'normal',
                                    seriesToggleReplot: { resetAxes: true }
                                }
                            },
                            cursor: {
                                zoom: true,
                                tooltipOffset: 10,
                                tooltipLocation: 'nw'
                            },
                            highlighter: {
                                sizeAdjust: 10,
                                tooltipLocation: 'n',
                                tooltipAxes: 'y',
                                tooltipFormatString: '<b><i><span style="color:red;">hello</span></i></b> %.2f',
                                useAxesFormatters: false
                            }
                        });

                }
            }
        });
    }

    $('#submit').click(function (e) {
        e.preventDefault();
        var baseCurrency = $('#baseCurrency').val();
        var targetCurrency = $('#targetCurrency').val();
        var start = $('#start').val();
        var end = $('#end').val();
        baseCurrency = encodeURIComponent(baseCurrency);
        start = encodeURIComponent(start);
        end = encodeURIComponent(end);
        targetCurrency = encodeURIComponent(targetCurrency);
        GetExchangeRates(baseCurrency, start, end, targetCurrency);
    });

});