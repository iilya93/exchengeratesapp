﻿using System.Collections.Generic;


namespace BLL.Models.Api
{
    public class ApiCurrencyModel
    {
        public Dictionary<string, decimal> Currencies { get; set; }
    }
}
