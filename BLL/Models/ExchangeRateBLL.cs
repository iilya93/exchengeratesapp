﻿using System;
using System.Collections.Generic;

namespace BLL.Models
{
    public class ExchangeRateBLL
    {
        public DateTime Date { get; set; }

        public string BaseNameIso { get; set; }

        public List<Rate> Rates { get; set; }
    }

    public class Rate
    {
        public string NameIso { get; set; }

        public decimal Value { get; set; }
    }
}
