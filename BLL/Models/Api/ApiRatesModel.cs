﻿using System;
using System.Collections.Generic;

namespace BLL.Models.Api
{
    public class ApiRatesModel
    {
        public DateTime Date { get; set; }

        public string Base { get; set; }

        public Dictionary<string, decimal> Rates { get; set; }
    }
}
