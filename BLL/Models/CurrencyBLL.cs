﻿

namespace BLL.Models
{
    public class CurrencyBLL
    {
        public string NameIso { get; set; }

        public string FullName { get; set; }
    }
}
