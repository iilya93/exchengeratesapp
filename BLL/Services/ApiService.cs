﻿using BLL.Interfaces;
using BLL.Models.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ApiService : IOpenExchangeRatesApi, IDisposable
    {
        private readonly string _baseUrl;
        private readonly string _idAccount;
        private readonly string _urlHistory;
        private readonly string _urlCurrency;


        private readonly HttpClient _client = null;


        public ApiService(string baseUrl, string idAccount, string urlHistory, string urlCurrency)
        {
            _baseUrl = baseUrl;
            _idAccount = idAccount;
            _urlHistory = urlHistory;
            _urlCurrency = urlCurrency;

            _client = new HttpClient();

            if (baseUrl != null)
            {
                _client.BaseAddress = new Uri(baseUrl);
            }

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
        }

        /// <summary>
        /// Выгрузка данных из Api на конкретную дату
        /// </summary>
        /// <param name="currencyNameIso"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public async Task<ApiRatesModel> GetHistoryAsync(string currencyNameIso, DateTime dateTime)
        {
            string parametrsGet = string.Format(@"{0}{1}.json?app_id={2}&base={3}&prettyprint=false"
                , WebUtility.HtmlEncode(_urlHistory)
                , WebUtility.HtmlEncode(dateTime.ToString("yyyy-MM-dd"))
                , WebUtility.HtmlEncode(_idAccount)
                , WebUtility.HtmlEncode(currencyNameIso));

            var result = await _client.GetAsync(parametrsGet);

            var resStrList = await result.Content.ReadAsStringAsync();

            var resultApi = JsonConvert.DeserializeObject<Dictionary<string, object>>(resStrList);

            if (!resultApi.ContainsKey("rates") && resultApi.ContainsKey("error") && (bool)resultApi["error"])
            {
                throw new Exception("status: " + resultApi["status"].ToString() + Environment.NewLine + "message: " + resultApi["message"].ToString() + Environment.NewLine + "description: " + resultApi["description"].ToString());
            }

            var rates = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(resultApi["rates"].ToString().Replace("/n/v", ""));

            ApiRatesModel apiRatesModel = new ApiRatesModel
            {
                Date = dateTime,
                Base = resultApi["base"].ToString(),
                Rates = rates
            };

            return apiRatesModel;
        }

        /// <summary>
        /// Выгрузка списка валют
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> GetCurrencies()
        {
            string parametrsGet = string.Format(@"{0}?app_id={1}&prettyprint=false"
               , WebUtility.HtmlEncode(_urlCurrency)
               , WebUtility.HtmlEncode(_idAccount));

            var result = await _client.GetAsync(parametrsGet);

            var resStrList = await result.Content.ReadAsStringAsync();

            var rates = JsonConvert.DeserializeObject<Dictionary<string, string>>(resStrList.ToString().Replace("/n/v", ""));

            return rates;
        }


        public void Dispose()
        {
            if (_client != null)
            {
                _client.Dispose();
            }
        }
    }
}
