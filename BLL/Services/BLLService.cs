﻿using BLL.Interfaces;
using BLL.Models;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class BLLService : IBLLService
    {
        private readonly IOpenExchangeRatesApi _openExchangeRatesApi;
        private readonly IDbService _dbService;

        public BLLService(IOpenExchangeRatesApi openExchangeRatesApi, IDbService dbService)
        {
            _openExchangeRatesApi = openExchangeRatesApi;
            _dbService = dbService;
        }

        /// <summary>
        /// Поиск списка валют. Асинхронный метод.
        /// </summary>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<List<CurrencyBLL>> GetCurrensiesAsync()
        {
            //Поиск в БД
            var currencies = _dbService.GetCurrencies();
            if (currencies == null || currencies.Count == 0)
            {
                //Если валют нет в БД, то загрузка из WebApi
                currencies = new List<DAL.Models.Currency>();
                var currenciesApi = await _openExchangeRatesApi.GetCurrencies();

                currencies.AddRange(currenciesApi.Select(c => new Currency { NameIso = c.Key, Fullname = c.Value }));

                await _dbService.AddCurrenciesAsync(currencies);
            }

            if (currencies.Count == 0)
            {
                throw new Exception("Не удалось получить валюты!");
            }

            //конвертация из типа БД в тип уровня бизнес логики
            List<CurrencyBLL> currencyBLLs = currencies.Select(c => new CurrencyBLL { NameIso = c.NameIso, FullName = c.Fullname }).ToList();

            return currencyBLLs;
        }

        /// <summary>
        /// Получение курса валют. Асинхронный метод
        /// </summary>
        /// <param name="baseNameIso"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task<List<ExchangeRateBLL>> GetExchangeRateAsync(string baseNameIso, DateTime start, DateTime end, string targetNameIso = null)
        {
            List<ExchangeRateBLL> list = new List<ExchangeRateBLL>();

            for (DateTime i = start; i <= end; i = i.AddDays(1))
            {
                var data = _dbService.GetExchangeRateForDate(baseNameIso, i, targetNameIso);

                if (data == null)
                {
                    var dataApi = _openExchangeRatesApi.GetHistoryAsync(baseNameIso, i).Result;


                    if (targetNameIso == null)
                    {
                        data = dataApi.Rates.Select(r => new ExchangeRate { CurrencyNameIso = r.Key, RateValue = r.Value }).ToList();

                        await _dbService.AddExchangeRateListForDateAsync(data, dataApi.Base, dataApi.Date);

                    }
                    else
                    {
                        var dataSave = dataApi.Rates.Select(r => new ExchangeRate { CurrencyNameIso = r.Key, RateValue = r.Value }).ToList();

                        await _dbService.AddExchangeRateListForDateAsync(dataSave, dataApi.Base, dataApi.Date);

                        data = dataApi.Rates.Where(r => r.Key == targetNameIso).Select(r => new ExchangeRate { CurrencyNameIso = r.Key, RateValue = r.Value }).ToList();

                    }

                }

                list.Add(new ExchangeRateBLL
                {
                    BaseNameIso = baseNameIso,
                    Date = i,
                    Rates = data.Select(r => new Rate { NameIso = r.CurrencyNameIso, Value = r.RateValue }).ToList()
                });

            }

            return list;
        }
    }
}
