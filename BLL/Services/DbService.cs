﻿using BLL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class DbService : IDbService
    {
        private readonly ExchangeRateDbContext _dbContext;
        public DbService(ExchangeRateDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Добавить валюту
        /// </summary>
        /// <param name="currencyNameIso"></param>
        /// <param name="currencyFullName"></param>
        public Currency AddCurrency(string currencyNameIso, string currencyFullName)
        {
            Currency currency = null;
            if (_dbContext.Currencies.FirstOrDefault(c => c.NameIso == currencyNameIso) == null)
            {
                currency = _dbContext.Currencies.Add(new Currency { Fullname = currencyFullName, NameIso = currencyNameIso }).Entity;
                _dbContext.SaveChanges();
            }

            return currency;
        }

        public async System.Threading.Tasks.Task AddCurrenciesAsync(IEnumerable<Currency> currencies)
        {
            foreach (var currency in currencies)
            {
                if (_dbContext.Currencies.FirstOrDefault(c => c.NameIso == currency.NameIso) == null)
                {
                    await _dbContext.Currencies.AddAsync(currency);
                }
            }
            await _dbContext.SaveChangesAsync();
        }


        /// <summary>
        /// Получить список валют из БД
        /// </summary>
        /// <returns></returns>
        public List<Currency> GetCurrencies()
        {
            if (_dbContext.Currencies.Count() != 0)
                return _dbContext.Currencies.OrderBy(c => c.NameIso).ToList();
            return null;
        }


        /// <summary>
        /// Получить валюту по имени
        /// </summary>
        /// <param name="nameIso"></param>
        /// <returns></returns>
        public Currency GetCurrency(string nameIso)
        {
            if (nameIso == null) throw new ArgumentNullException("nameIso отсутствует!");

            return _dbContext.Currencies.FirstOrDefault(c => c.NameIso == nameIso);
        }

        /// <summary>
        /// Изменения курса на конкретную дату из БД
        /// </summary>
        /// <param name="nameIso"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public List<ExchangeRate> GetExchangeRateForDate(string nameIso, DateTime date, string target = null)
        {
            if (nameIso == null)
                throw new ArgumentNullException("nameIso отсутствует!");
            List<ExchangeRate> data = null;

            if (target == null)
                data = _dbContext.CurrencyDates.FirstOrDefault(c => c.CurrencyNameIso == nameIso && c.Date == date)?.ExchangeRates;
            else
            {
                var dataValue = _dbContext.ExchangeRates.FirstOrDefault(e => e.CurrencyDate.CurrencyNameIso == nameIso && e.CurrencyDate.Date == date && e.CurrencyNameIso == target);

                if (dataValue != null)
                {
                    data = new List<ExchangeRate>() { dataValue };
                }
            }

            return data;
        }

        public List<ExchangeRate> AddExchangeRateListForDate(IEnumerable<ExchangeRate> exchangeRates, string nameIso, DateTime date)
        {
            var currencyDate = _dbContext.CurrencyDates.FirstOrDefault(c => c.CurrencyNameIso == nameIso && c.Date == date);

            if (currencyDate == null)
            {
                currencyDate = _dbContext.CurrencyDates.Add(new CurrencyDate { CurrencyNameIso = nameIso, Date = date }).Entity;

                _dbContext.SaveChanges();
            }

            foreach (var item in exchangeRates)
            {
                currencyDate.ExchangeRates.Add(new ExchangeRate { CurrencyNameIso = item.CurrencyNameIso, RateValue = item.RateValue });
                _dbContext.SaveChanges();
            }

            return currencyDate.ExchangeRates.ToList();
        }


        public async System.Threading.Tasks.Task AddExchangeRateListForDateAsync(IEnumerable<ExchangeRate> exchangeRates, string nameIso, DateTime date)
        {
            var currencyDate = _dbContext.CurrencyDates.FirstOrDefault(c => c.CurrencyNameIso == nameIso && c.Date == date);

            if (currencyDate != null)
            {
                return;
            }

            currencyDate = _dbContext.CurrencyDates.Add(new CurrencyDate { CurrencyNameIso = nameIso, Date = date }).Entity;

            foreach (var item in exchangeRates)
            {
                if (_dbContext.Currencies.FirstOrDefault(c => c.NameIso == item.CurrencyNameIso) == null)
                    _dbContext.Currencies.Add(new Currency { NameIso = item.CurrencyNameIso });

                currencyDate.ExchangeRates.Add(new ExchangeRate { CurrencyNameIso = item.CurrencyNameIso, RateValue = item.RateValue });
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}
