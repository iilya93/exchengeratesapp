﻿using ExchengeRatesApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchengeRatesApp.Services
{
    public interface IRateService
    {
        Task<RateValueViewModel> GetRateAsync(string baseCurrency, string target, DateTime start, DateTime end);

        Task<List<CurrencyViewModel>> GetCurrenciesAsync();
    }
}
