﻿using BLL.Models;
using System;
using System.Collections.Generic;


namespace BLL.Interfaces
{
    public interface IBLLService
    {
        System.Threading.Tasks.Task<List<CurrencyBLL>> GetCurrensiesAsync();

        System.Threading.Tasks.Task<List<ExchangeRateBLL>> GetExchangeRateAsync(string baseNameIso, DateTime start, DateTime end, string targetNameIso = null);
    }
}
