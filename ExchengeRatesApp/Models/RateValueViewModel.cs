﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExchengeRatesApp.Models
{
    public class RateValueViewModel
    {
        public string BaseNameIso { get; set; }

        public List<RateValueAndDateViewModel> Values { get; set; }

        public bool IsError { get; set; } = false;

        public string Message { get; set; }

    }

    public class RateValueAndDateViewModel
    {
        public string NameIso { get; set; }

        public DateTime Date { get; set; }

        public decimal Value { get; set; }
    }
}
