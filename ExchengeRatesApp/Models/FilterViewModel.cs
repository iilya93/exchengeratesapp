﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExchengeRatesApp.Models
{
    public class FilterViewModel
    {
        [Required(ErrorMessage = "Не указана базовая валюта")]
        public string BaseCurrency { get; set; }

        [Required(ErrorMessage = "Не указана целевая валюта")]
        public string TargetCurrency { get; set; }

        [Required(ErrorMessage = "Не указана начальная дата")]
        public DateTime Start { get; set; }

        [Required(ErrorMessage = "Не указана конечная дата")]
        public DateTime End { get; set; }
    }
}
