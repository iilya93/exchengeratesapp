﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class ExchangeRate
    {
        public int CurrencyDateId { get; set; }
        public virtual CurrencyDate CurrencyDate { get; set; }

        public string CurrencyNameIso { get; set; }
        public virtual Currency Currency { get; set; }

        public decimal RateValue { get; set; }
    }
}
