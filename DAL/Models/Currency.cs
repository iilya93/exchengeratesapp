﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
    public class Currency
    {
        public string NameIso { get; set; }

        public string Fullname { get; set; }

        public virtual List<CurrencyDate> CurrencyDates { get; set; }

        public Currency()
        {
            CurrencyDates = new List<CurrencyDate>();
        }
    }
}
