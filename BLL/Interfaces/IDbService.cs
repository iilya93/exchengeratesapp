﻿using DAL.Models;
using System;
using System.Collections.Generic;


namespace BLL.Interfaces
{
    public interface IDbService
    {
        Currency AddCurrency(string currencyNameIso, string currencyFullName);
        System.Threading.Tasks.Task AddCurrenciesAsync(IEnumerable<Currency> currencies);

        List<Currency> GetCurrencies();
        Currency GetCurrency(string nameIso);

        List<ExchangeRate> GetExchangeRateForDate(string nameIso, DateTime date, string target = null);
        List<ExchangeRate> AddExchangeRateListForDate(IEnumerable<ExchangeRate> exchangeRates, string nameIso, DateTime date);
        System.Threading.Tasks.Task AddExchangeRateListForDateAsync(IEnumerable<ExchangeRate> exchangeRates, string nameIso, DateTime date);
    }
}
