﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Interfaces;
using BLL.Models;
using ExchengeRatesApp.Models;

namespace ExchengeRatesApp.Services
{
    public class RateService : IRateService
    {
        private readonly IBLLService _bllService;

        public RateService(IBLLService bllService)
        {
            _bllService = bllService;
        }

        public async Task<List<CurrencyViewModel>> GetCurrenciesAsync()
        {
            var currenciesBll = await _bllService.GetCurrensiesAsync();
            return currenciesBll.Select(c => new CurrencyViewModel { NameIso = c.NameIso }).ToList();
        }

        /// <summary>
        /// Асинхронный метод получения ставок валют
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public async Task<RateValueViewModel> GetRateAsync(string baseCurrency, string target, DateTime start, DateTime end)
        {
            RateValueViewModel result = new RateValueViewModel { BaseNameIso = baseCurrency };

            List<ExchangeRateBLL> dataBLL = null;
            try
            {
                dataBLL = await _bllService.GetExchangeRateAsync(baseCurrency, start, end, target);
        }
            catch (AggregateException ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }


            if (dataBLL != null)
            {
                List<RateValueAndDateViewModel> values = new List<RateValueAndDateViewModel>();

                string[] lines = new string[dataBLL.Max(d => d.Rates.Count)];

                foreach (var item in dataBLL)
                {
                    values.AddRange(item.Rates.Select(d => new RateValueAndDateViewModel { Date = item.Date, NameIso = d.NameIso, Value = d.Value }));
                }

                result.Values = values;
            }

            return result;
        }
    }
}
