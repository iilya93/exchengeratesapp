﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ExchengeRatesApp.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using ExchengeRatesApp.Services;

namespace ExchengeRatesApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRateService _rateService;

        public HomeController(IRateService rateService)
        {
            _rateService = rateService;
        }

        public async Task<IActionResult> Index()
        {
            List<CurrencyViewModel> currencies = await _rateService.GetCurrenciesAsync();

            var allCurrency = new CurrencyViewModel { NameIso = "All" };

            List<CurrencyViewModel> targetCurrencies = new List<CurrencyViewModel>() { allCurrency };
            targetCurrencies.AddRange(currencies);

            var usd = currencies.First(c => c.NameIso == "USD").NameIso;

            ViewBag.Currencies = new SelectList(currencies, "NameIso", "NameIso", usd);

            ViewBag.TargetCurrencies = new SelectList(targetCurrencies, "NameIso", "NameIso", allCurrency);

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetExchangeRates(FilterViewModel filter)
        {
            if (ModelState.IsValid && filter.Start <= filter.End && (filter.End - filter.Start).TotalDays <= 62)
            {
                var target = filter.TargetCurrency == null || filter.TargetCurrency == "All" ? null : filter.TargetCurrency;

                var result = await _rateService.GetRateAsync(filter.BaseCurrency, target, filter.Start, filter.End);

                if (!result.IsError && result.Values.Count > 0)
                {
                    var lines = result.Values.Select(v => v.NameIso).Distinct().OrderBy(v => v).ToList();

                    var dates = result.Values.Select(v => v.Date).Distinct().OrderBy(v => v).ToList();


                    object[][][] values = new object[lines.Count()][][];

                    for (int i = 0; i < lines.Count(); i++)
                    {
                        values[i] = new object[dates.Count()][];

                        for (int y = 0; y < dates.Count(); y++)
                        {
                            values[i][y] = new object[2];
                            values[i][y][0] = dates[y].ToString("yyyy-MM-dd");
                            values[i][y][1] = result.Values.FirstOrDefault(v => v.NameIso == lines[i] && v.Date == dates[y])?.Value;
                        }
                    }

                    var title = string.Format("Exchange rates ({0} - {1})", filter.Start.ToString("yyyy-MM-dd"), filter.End.ToString("yyyy-MM-dd"));

                    return Json(
                        new
                        {
                            Lines = lines,
                            Values = values,
                            BaseCurrency = result.BaseNameIso,
                            Title = title,
                            IsError = result.IsError,
                            Message = result.Message
                        });
                }
                else
                {
                    if (result.IsError)
                    {
                        return Json(
                            new
                            {
                                IsError = result.IsError,
                                Message = result.Message
                            });
                    }

                    return Json(
                            new
                            {
                                IsError = true,
                                Message = "Данные отсутствуют"
                            });
                }
            }

            var errors = new System.Text.StringBuilder();

            if (filter.BaseCurrency == null)
            {
                errors.AppendLine("Base: не указан; ");
            }

            var d = new DateTime();

            if (filter.Start == d)
            {
                errors.AppendLine("Start: не указан; ");
            }

            if (filter.End == d)
            {
                errors.AppendLine("Finish: не указан; ");
            }

            if (filter.Start > filter.End)
            {
                errors.AppendLine("Finish < Start; ");
            }

            if ((filter.End - filter.Start).TotalDays > 62)
            {
                errors.AppendLine("Interval over 62 days; ");
            }


            return Json(
                    new
                    {
                        IsError = true,
                        Message = errors.ToString()
                    });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
