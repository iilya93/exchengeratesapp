﻿using BLL.Models.Api;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IOpenExchangeRatesApi
    {
        Task<ApiRatesModel> GetHistoryAsync(string currencyNameIso, DateTime dateTime);
        Task<Dictionary<string, string>> GetCurrencies();
    }
}
