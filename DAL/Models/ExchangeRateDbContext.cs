﻿using Microsoft.EntityFrameworkCore;

namespace DAL.Models
{
    public class ExchangeRateDbContext : DbContext
    {
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<CurrencyDate> CurrencyDates { get; set; }
        public DbSet<ExchangeRate> ExchangeRates { get; set; }

        public ExchangeRateDbContext(DbContextOptions<ExchangeRateDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Currency>(entity =>
            {
                entity.HasKey(c => c.NameIso);

                entity.Property(e => e.NameIso)
                .HasMaxLength(3)
                .IsUnicode(false);

                entity.Property(e => e.Fullname)
                .IsUnicode(false);
            });

            modelBuilder.Entity<CurrencyDate>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.HasIndex(c => new { c.CurrencyNameIso, c.Date });
            });

            modelBuilder.Entity<ExchangeRate>(entity =>
            {
                entity.HasKey(er => new { er.CurrencyDateId, er.CurrencyNameIso });
            });

        }
    }
}
