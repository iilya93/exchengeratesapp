﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public class CurrencyDate
    {
        public int Id { get; set; }

        public string CurrencyNameIso { get; set; }
        public virtual Currency Currency { get; set; }

        public DateTime Date { get; set; }

        public virtual List<ExchangeRate> ExchangeRates { get; set; }

        public CurrencyDate()
        {
            ExchangeRates = new List<ExchangeRate>();
        }
    }
}
